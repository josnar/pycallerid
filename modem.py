"""
    Copyright(c) 2014 Jose Ignacio Naranjo <joseignacio.naranjo@gmail.com>

    This file is part of PyCallerID.

    PyCallerID is free software: you can redistribute it and/or modify
    it under the terms of the Affero GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PyCallerID is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PyCallerID.  If not, see <http://www.gnu.org/licenses/>.
"""

import serial, logging
from settings import *
import states


class Modem(object):
    def __init__(self):
        self.__state = states.Waiting()
        self.__instance = serial.Serial(port=MODEM_PORT,
                                        baudrate=MODEM_BAUDRATE,
                                        bytesize=MODEM_BYTESIZE,
                                        stopbits=MODEM_STOPBITS,
                                        xonxoff=MODEM_FLOWCONTROL)

    def __activate_caller_id(self):
        wait = True
        logging.info("Activating caller id... ")
        
        self.__instance.write("AT+VCID=1\r\n")

        while wait:
            response = self.__instance.readline().strip()
            if response == "ERROR":
                sys.exit(1)
            elif response == "OK":
                wait = False

    def __listen(self):
        while True:
            response = self.__instance.readline().strip()
            logging.info("Message got from modem line: %s" % response)
            self.__state = self.__state.next(response)
            self.__state.run()

    def block_call(self):
        logging.info("Blocking call... ")
        self.__instance.write("ATA\r\n")
        self.__instance.write("ATH0\r\n")
        logging.info("Blocking commands issued")


    def close_modem(self):
        if self.__instance.isOpen():
            self.__instance.close()

    def run_modem(self):
        logging.info("Open serial port %s", MODEM_PORT)
        
        if self.__instance and not self.__instance.isOpen():
            self.__instance.open()
        if not self.__instance.isOpen():
            logging.error("Can't open the modem")
            sys.exit(1)

        logging.info("Modem up and running...")
        self.__activate_caller_id()
        logging.info("Activating state machine...")
        self.__listen()
