"""
    Copyright(c) 2014 Jose Ignacio Naranjo <joseignacio.naranjo@gmail.com>

    This file is part of PyCallerID.

    PyCallerID is free software: you can redistribute it and/or modify
    it under the terms of the Affero GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PyCallerID is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PyCallerID.  If not, see <http://www.gnu.org/licenses/>.
"""

import logging

from settings import DATABASE_FILE
import modem
import gcm_push
from persistent_list import ListPersistentOnFile

blocked_numbers = ListPersistentOnFile(DATABASE_FILE)     # List of blocked numbers. 

def call_received(number):
    global blocked_numbers

    if number in blocked_numbers:
        # Block call
        logging.info("Blocking call from number: %s" % number)
        modem.block_call()
    else:
        # Allow call. Don't do anything
        logging.info("Call allowed from number: %s" % number)
    gcm_push.send_message_call(number)

def block_number(number):
    global blocked_numbers
    blocked_numbers.append(number)

def unblock_number(number):
    global blocked_numbers
    blocked_numbers.remove(number)
