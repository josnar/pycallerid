"""
    Copyright(c) 2014 Jose Ignacio Naranjo <joseignacio.naranjo@gmail.com>

    This file is part of PyCallerID.

    PyCAllerID is free software: you can redistribute it and/or modify
    it under the terms of the Affero GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
"""

import logging

class ListPersistentOnFile(list):
    # TODO: document this class
    """ No entry can be duplicated """

    def __init__(self, file_path):
        super(ListPersistentOnFile, self).__init__()
        
        self.file_path = file_path
        try:
            file = open(file_path, 'r')
            for entry in file.read().splitlines(): # to avoid end of line character
                logging.info("[ListPersistentOnFile] entry: %s" % entry)
                super(ListPersistentOnFile, self).append(entry)
            file.close()
        except IOError:
            logging.info("File for the list does not exist. No import")

    def append(self, entry):
        logging.info("[ListPersistentOnFile] adding entry: %s" % entry)
        if entry in self:
            logging.info("[ListPersistentOnFile] entry already exist: %s" % entry)
            return

        # if an entry is not already registered, write it to the file
        super(ListPersistentOnFile, self).append(entry)
        file = open(self.file_path, 'a')
        file.write(entry)
        file.write('\n')
        file.close()        

    def remove(self, entry):
        logging.info("[ListPersistentOnFile] Deleting an enty: %s" % entry)

        if entry in self:
            # list remove entry
            super(ListPersistentOnFile, self).remove(entry)

            # item found. so remove it from the file
            file = open(self.file_path, 'r')
            lines = file.readlines()
            file.close()
            file = open(self.file_path, 'w')
            for line in lines:
                if entry not in line:
                    file.write(line)
            file.close()
        else:
            # TODO: create an own exception
            raise Exception
