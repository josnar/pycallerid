"""
    Copyright(c) 2014 Jose Ignacio Naranjo <joseignacio.naranjo@gmail.com>

    This file is part of PyCallerID.

    PyCallerID is free software: you can redistribute it and/or modify
    it under the terms of the Affero GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PyCallerID is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PyCallerID.  If not, see <http://www.gnu.org/licenses/>.
"""

import logging
import callhandler

SEPARATOR = " = "

# TODO: If at any state, we get a RING, we should go to Waiting I guess

class State(object):
    def run(self):
        assert 0, "run not implemented"

    def next(self, input):
        assert 0, "next not implemented"

class Waiting(State):
    def run(self):
        logging.info("State Waiting")

    def next(self, input):
        if "NMBR" in input:
            number_pos = input.find(SEPARATOR)+len(SEPARATOR)
            return ReceivingCallNumber(input[number_pos:])
        return self

class ReceivingCallNumber(State):
    def __init__(self, number):
        self.__number = number

    def run(self):
        logging.info("State Receiving Call. Getting Number")
        logging.info("State Receiving Call. Notify the caller ID event")
        callhandler.call_received(self.__number)

    def next(self, input):
        return Waiting()
