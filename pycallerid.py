#!/usr/bin/env python

"""
    Copyright(c) 2014 Jose Ignacio Naranjo <joseignacio.naranjo@gmail.com>

    This file is part of PyCallerID.

    PyCallerID is free software: you can redistribute it and/or modify
    it under the terms of the Affero GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PyCallerID is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PyCallerID.  If not, see <http://www.gnu.org/licenses/>.
"""

from modem import Modem
from server import ServerThread
import gcm_push
import persistent_list
import settings

import atexit, logging
import sys

from optparse import OptionParser

if __name__ == '__main__':
    usage = "usage: %prog [-k|--key] YOU_API_KEY [-p|--port] PORT"
    parser = OptionParser(usage=usage)
    parser.add_option("-k", "--key", action="store", dest="api_key",
                      help="API_KEY authorized for the application",
                      type="string")
    parser.add_option("-l", "--log", action="store", dest="path_log", default="/dev/stdout",
                      help="""Path where the log will be stored. By default log will be displayed
                      using the standard output""",
                      type="string")
    parser.add_option("-p", "--port", action="store", dest="port",
                      help="""Port where the server will be listening""",
                      type="int")
    
    (options, args) = parser.parse_args()
    if not options.api_key:
        parser.error('API_KEY not given')
    else:
        gcm_push.api_key = options.api_key

    if not options.port:
        parser.error('Port not given')

    gcm_push.reg_ids = persistent_list.ListPersistentOnFile(settings.REGISTRATION_IDS_FILE)

    # Remove all handlers just in case there is already any call to logging
    # because it will make basicConfig to not work
    root = logging.getLogger()
    if root.handlers:
        for handler in root.handlers:
            root.removeHandler(handler)
    logging.basicConfig(filename=options.path_log,
                        level=logging.DEBUG,
                        format='%(asctime)s %(levelname)-8s %(message)s')

    # create server thread to listen inbound queries
    server = ServerThread(settings.HOST, options.port)
    server.start()

    # start listening the line
    modem = Modem()
    modem.run_modem()
