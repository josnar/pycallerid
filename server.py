"""
    Copyright(c) 2014 Jose Ignacio Naranjo <joseignacio.naranjo@gmail.com>

    This file is part of PyCallerID.

    PyCallerID is free software: you can redistribute it and/or modify
    it under the terms of the Affero GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PyCallerID is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PyCallerID.  If not, see <http://www.gnu.org/licenses/>.
"""

import SocketServer
import logging
import threading
import unittest
import socket
import sys
import os
import ssl

import settings
import gcm_push
import persistent_list
import callhandler

class ServerHandler(SocketServer.StreamRequestHandler):
    def handle(self):
        data = self.rfile.readline().strip()
        logging.info("{} wrote: %s".format(self.client_address[0]) % data)
        message = Message(data)
        if message:
            message.execute(self)


class Server(SocketServer.TCPServer):
    def __init__(self, host, port):
        SocketServer.TCPServer.__init__(self, (host, port), ServerHandler)
        self.allow_reuse_address=True
        self.keyfile=settings.SSL_KEY_FILE
        self.certfile=settings.SSL_CERT_FILE
        self.ssl_version=ssl.PROTOCOL_TLSv1

    def get_request(self):
        newsocket, fromaddr = self.socket.accept()
        connstream = ssl.wrap_socket(newsocket,
                                     server_side=True,
                                     certfile = self.certfile,
                                     keyfile = self.keyfile,
                                     ssl_version = self.ssl_version)
        print connstream, fromaddr
        return connstream, fromaddr

class ServerThread(threading.Thread):
    def __init__(self, host, port):
        threading.Thread.__init__(self)
        self.server = Server(host, port)

    def run(self):
        logging.info("Opening server at: " + self.server.server_address.__str__())
        self.server.serve_forever()

    def shutdown(self):
        self.server.shutdown()

class Message(object):
    def __new__(self, data):
        # factory of Message's derived classes
        separator = data.find(':')
        command = data[0:separator]
        self.data = data[separator+1:]
        if command == 'add_registration_id':
            return super(Message, self).__new__(AddRegistrationIdMessage)
        elif command == 'get_blocked_numbers':
            return super(Message, self).__new__(GetBlockedNumbersMessage)
        elif command == 'add_blocked_number':
            return super(Message, self).__new__(AddBlockedNumberMessage)
        elif command == 'remove_blocked_number':
            return super(Message, self).__new__(RemoveBlockedNumberMessage)
        else:
            logging.error("Unknown command: %s" % command)
        return None


class AddRegistrationIdMessage(Message):
    def execute(self, socket=None):
        logging.info("Received message type: AddRegistrationIdMessage (%s)" % self.data)
        # decode the rest of the message
        reg_id = self.data
        # update list of registration ids
        gcm_push.reg_ids.append(reg_id)


class GetBlockedNumbersMessage(Message):
    def execute(self, socket):
        logging.info("Received message type: GetBlockedNumbersMessage")
        response = None
        if len(callhandler.blocked_numbers) == 0:
            response = None
        else:
            response = ':'.join([number for number in callhandler.blocked_numbers])
        logging.info("Response: %s" % response)
        socket.wfile.write(response)


class AddBlockedNumberMessage(Message):
    def execute(self, socket):
        logging.info("Received message type: AddBlockedNumber (%s)" % self.data)
        callhandler.block_number(self.data)


class RemoveBlockedNumberMessage(Message):
    def execute(self, socket):
        logging.info("Received message type: RemoveBlockedNumber (%s)" % self.data)
        callhandler.unblock_number(self.data)


class TestMessages(unittest.TestCase):
    def setUp(self):
        open(settings.REGISTRATION_IDS_FILE_TEST, 'w+')
        gcm_push.reg_ids = persistent_list.ListPersistentOnFile(settings.REGISTRATION_IDS_FILE_TEST)

    def tearDown(self):
        os.remove(settings.REGISTRATION_IDS_FILE_TEST)        

    def test_unknown_message(self):
        message = Message("unknown message")
        self.assertIsNone(message)

    def test_add_registration_id(self):
        message = Message("add_registration_id:fake_reg_id")
        self.assertIsInstance(message, AddRegistrationIdMessage)
        message.execute()
        self.assertEqual(len(gcm_push.reg_ids), 1)
        self.assertEqual(gcm_push.reg_ids[0], "fake_reg_id")

    def test_add_repeated_registration_id(self):
        message = Message("add_registration_id:fake_reg_id")
        self.assertIsInstance(message, AddRegistrationIdMessage)
        message.execute()
        self.assertEqual(len(gcm_push.reg_ids), 1)
        self.assertEqual(gcm_push.reg_ids[0], "fake_reg_id")

        # Reexecute to try to add it again
        message.execute()
        self.assertEqual(len(gcm_push.reg_ids), 1)
        self.assertEqual(gcm_push.reg_ids[0], "fake_reg_id")


class TestListBlockedNumbers(unittest.TestCase):
    def setUp(self):
        open(settings.DATABASE_FILE_TEST, 'w+')
        callhandler.blocked_numbers = persistent_list.ListPersistentOnFile(settings.DATABASE_FILE_TEST)
        self.server = ServerThread(settings.HOST, settings.PORT_TEST)
        self.server.start()

    def tearDown(self):
        os.remove(settings.DATABASE_FILE_TEST)
        self.server.shutdown()

    def test_get_blocked_numbers(self):
        for i in range(10):
            callhandler.blocked_numbers.append(str(i))

        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        # require a certificate from the server but it is not checked
        ssl_sock = ssl.wrap_socket(sock,
                                   cert_reqs=ssl.CERT_NONE)
        # Connect to server and send data
        ssl_sock.connect((settings.HOST, settings.PORT_TEST))
        ssl_sock.sendall("get_blocked_numbers:\n")

        # Receive data from the server and shut down
        received = ssl_sock.recv(1024)
        logging.info("Received: %s" % received)
        numbers = received.split(':')
        self.assertEqual(len(numbers), len(range(10)))


if __name__ == '__main__':
    # To send tcp packets with bash: echo "Hello" > /dev/tcp/192.168.1.100/9999
    root = logging.getLogger()
    if root.handlers:
        for handler in root.handlers:
            root.removeHandler(handler)
    logging.basicConfig(stream = sys.stdout,
                        level=logging.DEBUG,
                        format='%(asctime)s %(levelname)-8s %(message)s')

    loader = unittest.TestLoader()

    suite1 = loader.loadTestsFromTestCase(TestMessages)
    suite2 = loader.loadTestsFromTestCase(TestListBlockedNumbers)
    suites_list = [suite1, suite2]
    tests = unittest.TestSuite(suites_list)
    testResult = unittest.TextTestRunner(verbosity=2).run(tests)

    # server = ServerThread(settings.HOST, settings.PORT_TEST)
    # server.start()
