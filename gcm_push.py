"""
    Copyright(c) 2014 Jose Ignacio Naranjo <joseignacio.naranjo@gmail.com>

    This file is part of PyCallerID.

    PyCallerID is free software: you can redistribute it and/or modify
    it under the terms of the Affero GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PyCallerID is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PyCallerID.  If not, see <http://www.gnu.org/licenses/>.
"""

import logging
import unittest
import os
import sys

from gcm import GCM, gcm

import callhandler
import settings
import persistent_list

api_key = None   # API_KEY that will be needed to comminucate with GCM
reg_ids = None   # List of registration id. Use PersistentOnFile class

def send_message_call(number):
    global reg_ids, api_key
    gcm_client = GCM(api_key)
    data = { 'number': number}

    if len(reg_ids) == 0:
        logging.info("No registration id found. No one to notify")
        return

    try:
        response = gcm_client.json_request(registration_ids=reg_ids,
                                           data=data)
        logging.info(response)
        # Handling errors
        if 'errors' in response:
            logging.error("Errors received from GCM!!!")
            for error, reg_ids_aux in response['errors'].items():
                if error == 'NotRegistered':
                    logging.error("Some registration ids are NotRegistered: %s" % reg_ids_aux)
                    for reg_id in reg_ids_aux:
                        reg_ids.remove(reg_id)
        if 'canonical' in response:
            logging.info("Canonical answer")
            for reg_id, canonical_id in response['canonical'].items():
                logging.info("Replace: [%s -> %s]" % (reg_id, canonical_id))
                reg_ids.remove(reg_id)
                reg_ids.append(canonical_id)
        
    # TODO: handle errors making a class that inherits from GCM and overwriting json_request
    except gcm.GCMAuthenticationException, e:
        logging.error(e)

class TestRegistrationIds(unittest.TestCase):
    def setUp(self):
        open(settings.REGISTRATION_IDS_FILE_TEST, 'w+')
        self.test_reg_ids = persistent_list.ListPersistentOnFile(settings.REGISTRATION_IDS_FILE_TEST)

    def tearDown(self):
        os.remove(settings.REGISTRATION_IDS_FILE_TEST)

    def test_add_reg_ids(self):
        list_aux = ["regid1", "regid2"]
        for item in list_aux:
            self.test_reg_ids.append(item)
        self.assertItemsEqual(self.test_reg_ids, list_aux)

    def test_add_repeated_reg_ids(self):
        reg_id = "regid1"
        self.test_reg_ids.append(reg_id)
        self.assertEqual(len(self.test_reg_ids), 1)
        self.test_reg_ids.append(reg_id)
        self.assertEqual(len(self.test_reg_ids), 1)

    def test_delete_reg_ids(self):
        list_aux = ["regid1", "regid2"]
        for item in list_aux:
            self.test_reg_ids.append(item)
        self.assertEqual(len(self.test_reg_ids), 2)
        for item in list_aux:
            self.test_reg_ids.remove(item)
        self.assertEqual(len(self.test_reg_ids), 0)
        
    def test_delete_non_existant_reg_id(self):
        try:
            self.test_reg_ids.remove("fake")
            # Should not get here
            self.assertEqual(1, 0)
        except Exception, e:
            pass
        

if __name__ == '__main__':
    root = logging.getLogger()
    if root.handlers:
        for handler in root.handlers:
            root.removeHandler(handler)
    logging.basicConfig(stream = sys.stdout,
                        level=logging.DEBUG,
                        format='%(asctime)s %(levelname)-8s %(message)s')
    suite = unittest.TestLoader().loadTestsFromTestCase(TestRegistrationIds)
    testResult = unittest.TextTestRunner(verbosity=2).run(suite)
