from serial import EIGHTBITS, STOPBITS_ONE
import os
import socket

MODEM_PORT = "/dev/ttyACM0"
MODEM_BAUDRATE = 9600
MODEM_BYTESIZE = EIGHTBITS
MODEM_STOPBITS = STOPBITS_ONE
MODEM_FLOWCONTROL = False

PATH = os.path.dirname(os.path.realpath(__file__))

DATABASE_FILE = PATH + "/database.txt"
DATABASE_FILE_TEST = PATH + "/database_test.txt"
REGISTRATION_IDS_FILE = PATH + "/registration.txt"
REGISTRATION_IDS_FILE_TEST = PATH + "/registration_test.txt"

# openssl req -x509 -nodes -days 365 -newkey rsa:4096 -keyout server.key -out server.crt -reqexts v3_req -extensions v3_ca
# Android seems to allow only "der" format. This worked for me
# openssl x509 -in server.crt -outform der -out server.der.crt
# Information got from:
# http://android.stackexchange.com/questions/61540/self-signed-certificate-install-claims-success-but-android-acts-as-if-cert-isn

SSL_CERT_FILE = PATH + "/cert/server.crt"
SSL_KEY_FILE = PATH + "/cert/server.key"
HOST = socket.gethostbyname("%s.local" % socket.gethostname())
PORT_TEST = 9999
